package it.unimi.di.sweng.lab04;
	
import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;


public class StringCalculatorTest {
	
	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);
	
	Calculator calculator = null;
	
	@Before
	public void setUp(){ 
		calculator = new StringCalculator();
	}

	@Test
	public void simpleAdd() {
		assertThat(calculator.add("")).isEqualTo(0);
		assertThat(calculator.add("1")).isEqualTo(1);
		assertThat(calculator.add("1,2")).isEqualTo(3);
	}
	
	@Test
	public void arbitraryAdd() {
		assertThat(calculator.add("1,2,3,4")).isEqualTo(10);
	}
	
	@Test
	public void wrapAdd() {
		assertThat(calculator.add("1\n2,3")).isEqualTo(6);
	}
	
	@Test
	public void newSepAdd(){
		assertThat(calculator.add("//;\n1;2")).isEqualTo(3);
	}
	
	@Test
	public void illegalAdd(){
		try{
			calculator.add("-1,3,-2");
			failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
		}catch(IllegalArgumentException e){
			assertThat(e).hasMessage("Numeri negativi non ammessi: -1, -2");
		}
	}
	
	@Test
	public void thousandAdd() {
		assertThat(calculator.add("1001,2")).isEqualTo(2);
	}
	
	@Test
	public void longSepAdd(){
		assertThat(calculator.add("//[***]\n1***2***3")).isEqualTo(6);
	}
	
	@Test
	public void multiSepAdd(){
		assertThat(calculator.add("//[*][%]\n1*2%3")).isEqualTo(6);
	}
}