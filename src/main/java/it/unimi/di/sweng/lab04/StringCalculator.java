package it.unimi.di.sweng.lab04;

import java.util.regex.Pattern;

public class StringCalculator implements Calculator {

	private static final int MAXNUMBER = 1000;
	private String negatives = "";
	private String regex = ",|\n";

	@Override
	public int add(String expression) {
		
		int sum = 0;
		final boolean isEmpty = expression.length() == 0;
		final boolean startsWithDSlash = expression.startsWith("//");
		
		if(isEmpty)
			return sum;
		
		if(startsWithDSlash){
			expression = calculateSep(expression);
		}
		
		String numbers[] = expression.split(regex); 
		sum = sumNumber(sum, numbers);
		if(negatives.length() > 0){
			negatives = negatives.substring(0, negatives.length()-1);
			throw new IllegalArgumentException("Numeri negativi non ammessi:" + negatives);
		}
		
		return sum;
	}

	private String calculateSep(String expression) {
		int pos;
		expression = expression.substring(2); //Elimina "//"
		if(expression.charAt(0) != '['){
			regex += "|" + expression.charAt(0);
			expression = expression.substring(1); //Elimina singolo delimitatore
		}
		while(expression.charAt(0) == '['){
			expression = expression.substring(1); //Elimina "["
			pos = addMultiSep(expression);
			expression = expression.substring(pos+1);  //Elimina multiplo delimitatore
		}
		expression = expression.substring(1); //Elimina \n
		return expression;
	}

	private int addMultiSep(String expression) {
		int pos;
		regex += "|";
		for(pos = 0 ; expression.charAt(pos) != ']'; pos++)
			regex += Pattern.quote(expression.charAt(pos) + "");
		return pos;
	}
	
	private int sumNumber(int sum, String[] numbers) {
		int value, length = numbers.length;
		for(int i = 0; i < length; i++){
			value = Integer.valueOf(numbers[i]);
			if(value <= MAXNUMBER)
				sum += value;
			if(value < 0)
				negatives += " " + value + ",";
		}
		return sum;
	}
	
}